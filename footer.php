        <footer id="site-footer" role="contentinfo" class="site-footer">
            <div class="container text-center">
                <p>
                    <?php _e( 'Blog zasilany przez', 'blog-theme' ); ?>
                    <a target="_blank" href="<?php echo esc_url( __( 'https://wordpress.org/', 'blog-theme' ) ); ?>">
		                <?php _e( 'WordPress', 'blog-theme' ); ?> <?php echo $GLOBALS[ 'wp_version' ]; ?></a>,
	                <?php _e( 'oparty na motywie', 'blog-theme' ); ?>
                    <a href="<?php echo get_permalink( get_page_by_path( 'motyw' ) ); ?>">
		                <?php echo wp_get_theme()->get( 'Name' ); ?></a>.
                </p>
                <p class="mb-0">
                    &copy; <?php echo date( 'Y' ); ?> <a href="https://krzysztof.tatarynowicz.eu">Krzysztof Tatarynowicz</a>
                </p>
            </div>
        </footer>
    </div>
    <?php wp_footer(); ?>
</body>
</html>
