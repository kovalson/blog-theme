<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Blog Theme
 */

get_header();
?>

		<div id="primary" class="content-area site-content">
			<main id="main" class="site-main container">
				<?php if ( is_active_sidebar( 'sidebar-mobile' ) ): ?>
                    <aside id="secondary" class="widget-area sidebar d-block d-md-none mb-4" role="complementary">
						<?php dynamic_sidebar( 'sidebar-mobile' ); ?>
                    </aside>
				<?php endif; ?>
                <div class="row">
                    <div class="col-md-8">
                        <?php

                        // Start the loop.
                        while ( have_posts() ) {

                            the_post();

                            get_template_part( 'template-parts/content', 'page' );

                            // If comments are open or we have at least one comment, load up the comment template.
                            if ( comments_open() || get_comments_number() ) {
                                comments_template();
                            }

                        }

                        ?>
                    </div>
                    <div class="col-md-4">
                        <div class="sidebar-wrapper">
		                    <?php get_sidebar(); ?>
                        </div>
                    </div>
                </div>
			</main>
		</div>

<?php
get_footer();
