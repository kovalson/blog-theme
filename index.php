<?php
/**
 * The main template file.
 *
 * @package Wordpress
 * @subpackage blog-theme
 */

get_header();
?>

        <div id="site-content" class="site-content" role="main">
            <main id="main" class="site-main container">
	            <?php if ( is_active_sidebar( 'sidebar-mobile' ) ): ?>
                    <aside id="secondary" class="widget-area sidebar d-block d-md-none mb-4" role="complementary">
                        <?php dynamic_sidebar( 'sidebar-mobile' ); ?>
                    </aside>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-8 entries-separated">
                        <?php
                        if ( have_posts() ) {

                            while ( have_posts() ) {

                                the_post();

                                get_template_part( 'template-parts/content', get_post_format() );

                            }

                            bootstrap_pagination();

                        } else {

                            get_template_part( 'template-parts/content', 'none' );

                        }
                        ?>
                    </div>
                    <div class="col-md-4">
                        <div class="sidebar-wrapper">
                            <?php get_sidebar( 'sidebar' ); ?>
                        </div>
                    </div>
                </div>
            </main>
        </div>

<?php
get_footer();
