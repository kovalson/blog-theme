<?php
/**
 * Custom script loader class.
 */
require get_template_directory() . '/classes/class-blog-theme-script-loader.php';

/**
 * Custom template tags for the theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom bootstrap posts pagination.
 */
require get_template_directory() . '/inc/bootstrap-pagination.php';

/**
 * Better support for responsive images out of the box.
 */
require get_template_directory() . '/inc/images.php';

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function blog_theme_support() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Custom background color.
	add_theme_support(
		'custom-background',
		[
			'default_color' => '#e9ecef',
		],
	);

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Sample post thumbnail sizes. Change these to fit your theme
	 *
	 * @since  Starlight 1.1.0
	 */
	add_image_size( 'blog-theme-featured-large', 1920, 500, true );      // Featured image
	add_image_size( 'blog-theme-featured', 660 ); 					                // Featured image
	add_image_size( 'blog-theme-thumb-l', 640, 480 ); 				        // Thumbnail size large
	add_image_size( 'blog-theme-thumb-m', 255 ); 					                // Thumbnail size small

	// Set post thumbnail size.
	set_post_thumbnail_size( 1200, 9999 );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		[
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
			'navigation-widgets',
		]
	);

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', [
		'aside',
		'image',
		'video',
		'quote',
		'link',
	]);

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Twenty Twenty, use a find and replace
	 * to change 'blog-theme' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'blog-theme', get_template_directory() . '/languages' );

	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	// Add support for responsive embeds.
	add_theme_support( 'responsive-embeds' );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	// Add support for editor styles.
	add_theme_support( 'editor-styles' );

	/*
	 * Adds `async` and `defer` support for scripts registered or enqueued
	 * by the theme.
	 */
	$loader = new BlogTheme_Script_Loader();
	add_filter( 'script_loader_tag', [ $loader, 'filter_script_loader_tag' ], 10, 2 );

}

add_action( 'after_setup_theme', 'blog_theme_support' );

/**
 * Register theme sidebars.
 */
function blog_theme_register_sidebars() {

	register_sidebar([
		'name'          => __( 'Pasek boczny', 'blog-theme' ),
		'id'            => 'sidebar',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="sidebar-title">',
		'after_title'   => '</h2>',
	]);

	register_sidebar([
		'name'          => __( 'Sidebar w widoku mobilnym', 'blog-theme' ),
		'id'            => 'sidebar-mobile',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="sidebar-title">',
		'after_title'   => '</h2>',
	]);

}

add_action( 'widgets_init', 'blog_theme_register_sidebars' );

/**
 * Register and Enqueue Scripts.
 */
function blog_theme_register_scripts() {

	$theme_version = wp_get_theme()->get( 'Version' );

	if ( ( ! is_admin() ) && is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'blog-theme-app-js', get_template_directory_uri() . '/app.js', [], $theme_version );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}

add_action( 'wp_enqueue_scripts', 'blog_theme_register_scripts' );

/**
 * Register navigation menus uses wp_nav_menu in five places.
 */
function blog_theme_menus() {

	$locations = [
		'primary'  => __( 'Desktop Horizontal Menu', 'blog-theme' ),
		'expanded' => __( 'Desktop Expanded Menu', 'blog-theme' ),
		'mobile'   => __( 'Mobile Menu', 'blog-theme' ),
		'footer'   => __( 'Footer Menu', 'blog-theme' ),
		'social'   => __( 'Social Menu', 'blog-theme' ),
	];

	register_nav_menus( $locations );

}

add_action( 'init', 'blog_theme_menus' );

/**
 * Add filter for link class in nav menu.
 */
function add_menu_link_class($atts, $item, $args) {

	if ( property_exists( $args, 'link_class' ) ) {
		$atts['class'] = $args->link_class;
	}

	return $atts;

}

add_filter( 'nav_menu_link_attributes', 'add_menu_link_class', 1, 3 );
