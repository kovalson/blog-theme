<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Wordpress
 * @subpackage Blog Theme
 */

if ( ! is_active_sidebar( 'sidebar' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area sidebar" role="complementary">
	<?php dynamic_sidebar( 'sidebar' ); ?>
</aside>
