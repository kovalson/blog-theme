const path = require("path");

module.exports = {
    entry: path.resolve(__dirname, "./resources/js/index.js"),
    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                use: [
                    {
                        loader: "style-loader",
                    },
                    {
                        loader: "css-loader",
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            postcssOptions: {
                                plugins: function () {
                                    return [
                                        require("autoprefixer"),
                                    ];
                                },
                            },
                        },
                    },
                    {
                        loader: "sass-loader",
                    },
                ],
            },
        ],
    },
    output: {
        path: path.resolve(__dirname),
        filename: "app.js",
    },
};
