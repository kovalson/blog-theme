# Blog Theme

A custom theme for Wordpress blog.

This repository stores the source files for the theme. In order to use it on your Wordpress blog, you need to build
the CSS and JS files on your own.

## Build

### Production

If you're experienced in building and compiling projects, go ahead and run the [full production build](#ready-to-upload-theme-archive)
that generates the .MO translations and the whole archive automatically.

To build the theme, you will require [Node.js](https://nodejs.org/en/) (tested for v14.9.0). Then you need to run the
following command to install all dependencies required for the build to run:

```shell
npm install
```

When the installation is finished (without errors), you can build the project using the following command:

```shell
npm run prod
```

Note that these commands will only compile the source JS and SCSS files into a single `app.js` file. If this is the
final point of your acceptance for compiling process, you only need to make an archive so that Wordpress will be able to
load the theme. You can simply select required files and make an archive. Remember **NOT TO** include the following
files in your archive:

- `resources` - the whole directory is required only during the compilation,
- `.gitignore` - required for GIT,
- `package.json` - required to install and build the package,
- `README.md` - required for instructions,
- `webpack.config.js` - required to install and build the package,
- `wp-cli.phar` - if exists.

All other files are essential for the theme to work.

### Ready-to-upload theme archive

You may also run the `npm run full` command which will run the production and also create a ready-to-upload theme
archive for you! Note that you require `7z` executable to be present in your `PATH` variables in order to successfully
run this command. The new archive named `blog-theme.zip` will be then created in the main theme directory.

### Scripts

The following are the up-to-date npm scripts available.

#### `build`

If you want to build the project in `development` mode, you may use the `npm run build`. 

#### `watch`

This script is used mainly for development process, where the files are watched for changes and compiled immediately
in `development` mode.

#### `prod`

Compiles the JS and SCSS sources files into a single `app.js` file with `app.js.LICENSE.txt` file that includes LICENSES
for all third-party libraries used in the project.

#### `full`

First runs the `npm run prod` script, and then creates an archive including required theme files for Wordpress to load.

#### `translate`

Script required whenever there is any change in the `languages/blog-theme.POT` file. Basically, whenever you add or
remove any translation from the theme (translations are denoted by `_e()`, `__()` and `_x()` functions), you should
run this script and then update all other translations in the `languages/` directory. This way your blog can be properly
translated to other languages.
