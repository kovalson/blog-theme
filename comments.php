<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @package Wordpress
 * @subpackage Blog Theme
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) : ?>
		<h2 class="comments-title"><?php _e( 'Komentarze', 'blog-theme' ); ?></h2>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
				<h2 class="screen-reader-text"><?php esc_html_e( 'Nawigacja', 'blog-theme' ); ?></h2>
				<div class="nav-links">
					<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Starsze komentarze', 'blog-theme' ) ); ?></div>
					<div class="nav-next"><?php next_comments_link( esc_html__( 'Nowsze komentarze', 'blog-theme' ) ); ?></div>
				</div>
			</nav>
		<?php endif; ?>

		<ol class="comment-list">
			<?php
			wp_list_comments([
				'style'      => 'ol',
				'short_ping' => true,
			]);
			?>
		</ol>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
				<h2 class="screen-reader-text"><?php esc_html_e( 'Nawigacja', 'blog-theme' ); ?></h2>
				<div class="nav-links">
					<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Starsze komentarze', 'blog-theme' ) ); ?></div>
					<div class="nav-next"><?php next_comments_link( esc_html__( 'Nowsze komentarze', 'blog-theme' ) ); ?></div>
				</div>
			</nav>
		<?php
		endif;

	endif;


	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
		<p class="no-comments"><?php esc_html_e( 'Komentarze wyłączone.', 'blog-theme' ); ?></p>
	<?php
	endif;

	comment_form();
	?>

</div>
