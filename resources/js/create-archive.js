const fs = require("fs");
const Seven = require("node-7z");
const { mo, po } = require("gettext-parser");

const LANGUAGES_DIR = "languages/";

// The source files to archive
const source = [
    "classes/",
    "inc/",
    "languages/",
    "template-parts/",
    "app.js",
    "app.js.LICENSE.txt",
    "comments.php",
    "footer.php",
    "functions.php",
    "header.php",
    "index.php",
    "page.php",
    "screenshot.png",
    "sidebar.php",
    "single.php",
    "style.css",
];

// We get all files in languages directory
const files = fs.readdirSync(LANGUAGES_DIR);

// We compile all .po files into .mo files and save them inside that directory
files.forEach((fileName) => {
    if (fileName.endsWith(".po")) {
        const fileContents = fs.readFileSync(LANGUAGES_DIR + fileName);
        const compiledContents = mo.compile(po.parse(fileContents));
        const compiledFileName = LANGUAGES_DIR + fileName.replace(".po", ".mo");

        fs.appendFileSync(compiledFileName, compiledContents);
    }
});

// We create the archive
const addStream = Seven.add("./blog-theme.zip", source, {
    recursive: true,
    fullyQualifiedPaths: true,
});

addStream.on("end", () => {
    // The `node_modules` directory is includes as well because of the `classes` directory
    // in source files array. The easiest way to get rid of the modules is to delete them
    // after creating the archive
    const deleteStream = Seven.delete("./blog-theme.zip", "node_modules");

    deleteStream.on("end", () => {
        console.log("All files have been successfully archived.");

        // We want to delete all generated .mo files as they are only required by Wordpress
        const files = fs.readdirSync(LANGUAGES_DIR);
        files.forEach((fileName) => {
            if (fileName.endsWith(".mo")) {
                fs.unlinkSync(LANGUAGES_DIR + fileName);
            }
        });

        console.log("Generated .mo files successfully deleted.");
    });
});
