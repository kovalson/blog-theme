<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @package Wordpress
 * @subpackage Blog Theme
 */

?>

<div class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title display-4 fw-bold"><?php esc_html_e( 'Szukasz za daleko!', 'blog-theme' ); ?></h1>
	</header>

	<div class="page-content">
		<?php if ( is_search() ) : ?>

			<p class="lead"><?php esc_html_e( 'Niestety nie znaleziono niczego dla podanej frazy. Spróbuj wyszukać z innym słowem.', 'blog-theme' ); ?></p>

		<?php else : ?>

			<p class="lead"><?php esc_html_e( 'Niczego tu nie ma. Może spróbuj wyszukać?', 'blog-theme' ); ?></p>

		<?php endif; ?>
	</div>
</div>
