<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @package Wordpress
 * @subpackage Blog Theme
 */

get_header();
?>

		<div id="site-content" class="site-content" role="main">
			<main id="main" class="site-main container">
				<div class="row">
					<div class="col-xl-8 entries-separated">
						<?php
						if ( have_posts() ) {

							while ( have_posts() ) {

								the_post();

								get_template_part( 'template-parts/content', 'content' );

							}

						} else {

							get_template_part( 'template-parts/content', 'none' );

						}
						?>
					</div>
					<div class="col-xl-4 mt-3 mt-xl-0">
                        <div class="sidebar-wrapper">
							<?php get_sidebar(); ?>
                        </div>
					</div>
				</div>
			</main>
		</div>

<?php
get_footer();
