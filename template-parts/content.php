<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Blog Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry' . ( is_single() || is_page() ? ' single' : '' ) ); ?>>

	<?php if ( is_single() ): ?>

		<header class="entry-header">
			<?php if ( is_singular() ): ?>
                <div class="entry-categories">
					<?php the_category( ', ' ); ?>
                </div>
                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            <?php endif; ?>
		</header>

		<?php blog_theme_featured_image( 'blog-theme-featured-large' ); ?>

		<div class="entry-content">
			<?php
			the_content(
				sprintf(
					wp_kses(
						__( 'Czytaj dalej<span> "%s"</span>', 'blog-theme' ),
						[
							'span' => [
								'class' => [],
							],
						]
					),
					get_the_title()
				)
			);

			wp_link_pages([
				'before'    => '<div class="page-links">' . __( 'Strony:', 'blog-theme' ),
				'after'     => '</div>',
			]);
			?>
		</div>

		<footer class="entry-footer">
			<ul class="list-unstyled mb-0">
				<li><?php the_author(); ?></li>
				<li><?php blog_theme_posted_on(); ?></li>
			</ul>
		</footer>

	<?php else: ?>

		<?php if ( has_post_thumbnail() ) : ?>
            <div class="entry-compact-left">
				<?php blog_theme_featured_image(); ?>
            </div>
		<?php endif; ?>

        <div class="entry-compact-right">
            <header class="entry-header">
                <div class="entry-categories">
                    <?php

                    if ( has_category() ) {
                        the_category( ', ' );

                        echo ', ';
                    }

                    blog_theme_posted_on();

                    ?>
                </div>
                <?php
                if ( is_single() ) {
                    the_title( '<h1 class="entry-title">', '</h1>' );
                } else {
                    the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' );
                }
                ?>
            </header>

            <div class="entry-content">
                <?php

                the_excerpt();

                blog_theme_comments_number();

                ?>
            </div>
        </div>

	<?php endif; ?>

</article>
