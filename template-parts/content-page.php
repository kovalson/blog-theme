<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @package Wordpress
 * @subpackage Blog Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry' . ( is_single() || is_page() ? ' single' : '' ) ); ?>>
    <header class="entry-header">
        <div class="entry-categories">
		    <?php the_category( ', ' ); ?>
        </div>
        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
    </header>

	<?php blog_theme_featured_image( 'blog-theme-featured-large' ); ?>

	<div class="entry-content <?php if ( is_single() ): ?>entry-single<?php endif; ?>">
		<?php the_content(); ?>
	</div>

    <footer class="entry-footer">
        <ul class="list-unstyled mb-0">
            <li><?php the_author(); ?></li>
            <li><?php blog_theme_posted_on(); ?></li>
        </ul>
    </footer>
</article>

<?php blog_theme_tags(); ?>
