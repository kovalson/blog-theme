<?php
/**
 * Custom template tags for this theme
 *
 * @package WordPress
 * @subpackage Blog Theme
 */

if ( ! function_exists( 'blog_theme_posted_on' ) ) {
	function blog_theme_posted_on(): void {
		$postedTime = get_the_time( 'U' );
		$modifiedTime = get_the_modified_time( 'U' );

		printf(
			'<time datetime="%1$s">%2$s</time>',
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() )
		);

		if ($postedTime !== $modifiedTime) {
			printf(
				', ' . __( 'zaktualizowano', 'blog-theme' ) . ' <time datetime="%1$s">%2$s</time>',
				esc_attr( get_the_modified_date( DATE_W3C ) ),
				esc_html( get_the_modified_date() )
			);
		}
	}
}

if ( ! function_exists( 'blog_theme_comments_number' ) ) {
	function blog_theme_comments_number() {
		$commentsNumber = get_comments_number();

		if ($commentsNumber > 0) {
			printf(
				'<span aria-label="%1$s"><i class="fas fa-comments fa-fw me-2" aria-hidden="true"></i>%2$s</span>',
				__( 'Liczba komentarzy', 'blog-theme' ),
				get_comments_number()
			);
		}
	}
}

if ( ! function_exists( 'blog_theme_tags' ) ) {
	function blog_theme_tags() {

		$tags = get_the_tag_list();

		if ($tags) {
			printf(
				'<div class="entry-tags">%1$s</div>',
				$tags
			);
		}

	}
}
