<?php

/**
 * Prints out better responsive images. Uses the class "responsive-image"
 *
 * $url The image address
 * $size Name of the thumbnail size
 * $class Custom image class
 * $alt Alternative text if it can't be loaded
 */
function blog_theme_featured_image( $size = 'blog-theme-featured', $class = '', $attr = '', $echo = true ) {

	do_action( 'blog_theme_before_featured_image_func' );

	if ( ! $echo ) {
		return;
	}

	if ( ! has_post_thumbnail() ) {
		return;
	}

	if ( empty( $size ) ) {
		return;
	}

	if ( empty( $class ) ) {
		$class = '';
	}


	// Do not print it in the content if it's already in the header
	global $post;
	$page_thumbnail_large = get_the_post_thumbnail_url( $post->ID, 'page-thumbnail' );
	$page_thumbnail_size = blog_theme_has_large_featured_image( $post->ID );

	if ( 'large' === $page_thumbnail_size ) {
		return;
	}

	if ( ! is_single() ) {
		echo '<a href="' .  esc_url( get_the_permalink() ) . '">';
	}

	// Print the HTML of the image
	echo "<picture class='entry-featured-image ${class}'>";

	// use the standard featured image function
	the_post_thumbnail( $size, $attr );

	echo "</picture>";

	if ( ! is_single() ) {
		echo '</a>';
	}

}

/**
 * We are using very wide image size. If they don't have such do not show it, instead
 * print the featured image in the entry's post content. We will have default background
 * set from the CSS of the theme.
 */
function blog_theme_has_large_featured_image( $post_ID ) {

	// Grab the width of the page thumbnail.
	$image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post_ID ), 'blog-theme-featured-large' );
	$image_width = $image_data[1];

	// If the image they've provided isn't big enough use the smaller one in the content.
	if ( $image_width !== 1920 ) {
		return 'normal';
	}

	return 'large';

}
