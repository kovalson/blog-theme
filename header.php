<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="author" content="Krzysztof Tatarynowicz, ktatarynowicz@hotmail.com" />
	<meta name="robots" content="index, follow" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <div class="site-wrapper">
        <header id="site-header" class="site-header<?php if ( is_user_logged_in() ): ?> with-admin-navbar<?php endif; ?>" role="banner">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container-fluid px-0">
                        <a class="navbar-brand" href="<?php echo home_url(); ?>"><?php echo get_bloginfo( 'name' ); ?></a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <?php if ( has_nav_menu( 'primary' ) ): ?>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            <span><?php echo wp_get_nav_menu_name( 'primary' ); ?></span>
                                            <i class="fas fa-angle-down fa-fw ms-auto"></i>
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <?php
                                            wp_nav_menu([
                                                'theme_location' => 'primary',
                                                'container' => '',
                                                'items_wrap' => '%3$s',
                                                'link_class' => 'dropdown-item',
                                                'link_active_class' => 'active',
                                            ]);
                                        ?>
                                        </ul>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </header>
